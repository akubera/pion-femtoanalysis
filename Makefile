#
# Makefile
#


.PHONY: help
help:
	@echo "make <cmd> where cmd is the name of a macro found in run/"


%: run/%.C
	root -l -q $<+
