===============
Pion Femtoscopy
===============

Pion femtoscopy with the ALICE collaboration.


Usage
-----

Find 'runable' macros in the ``run/`` directory.

Use environment variables for setting parameters


A makefile is provided which will automatically run any macros found
in the run directory.
For example, to execute ``run/fit.C``, just type ``make fit``
and root will run.
